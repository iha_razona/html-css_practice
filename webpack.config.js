// config
const config = require('./config.js');
const entryScript = config.js.script;
const entryVendor = config.js.vendor;
const output = config.js.output;
const webpack = require('webpack');

module.exports = {
  entry: {
    script: entryScript,
    vendor: entryVendor
  },
  output: {
    path: output,
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                ['es2015', {'modules': false}]
              ]
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin()
  ]
}