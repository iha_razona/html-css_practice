/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 44);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var g;

// This works in non-strict mode
g = function () {
	return this;
}();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1, eval)("this");
} catch (e) {
	// This works if the window reference is available
	if ((typeof window === "undefined" ? "undefined" : _typeof(window)) === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* WEBPACK VAR INJECTION */(function(global) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modules_common__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__modules_accordion__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modules_change_image__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modules_footer__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modules_slider__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__modules_article_card__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modules_taglink__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modules_link__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__modules_match_height__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__modules_nav__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__modules_output_json__ = __webpack_require__(54);












// common
var common = new __WEBPACK_IMPORTED_MODULE_0__modules_common__["a" /* default */]();
common.init();
common.omitSentences(common.checkMedia());

// accordion
var accordion = new __WEBPACK_IMPORTED_MODULE_1__modules_accordion__["a" /* default */]();
accordion.init();
accordion.spAccordion(common.checkMedia());
accordion.spAccordion02(common.checkMedia());
accordion.accordionNav(common.checkMedia());

// changeImg
var changeImg = new __WEBPACK_IMPORTED_MODULE_2__modules_change_image__["a" /* default */]();
changeImg.change(common.checkMedia());
changeImg.change02(common.checkMedia());
changeImg.changeBg(common.checkMedia());

// footer
var footer = new __WEBPACK_IMPORTED_MODULE_3__modules_footer__["a" /* default */]();
footer.init();

// slider
var slider = new __WEBPACK_IMPORTED_MODULE_4__modules_slider__["a" /* default */]();
slider.init();

// taglink
var tagLink = new __WEBPACK_IMPORTED_MODULE_6__modules_taglink__["a" /* default */]();
tagLink.init();

// link
var link = new __WEBPACK_IMPORTED_MODULE_7__modules_link__["a" /* default */]();
link.init();

// matchHeight
var matchHeight = new __WEBPACK_IMPORTED_MODULE_8__modules_match_height__["a" /* default */]();
matchHeight.matchHeight(common.checkMedia());

// output_json
var output_json = new __WEBPACK_IMPORTED_MODULE_10__modules_output_json__["a" /* default */]();
$(window).on("load", function () {
    output_json.init();
});

// triangleView
var articleCard = new __WEBPACK_IMPORTED_MODULE_5__modules_article_card__["a" /* default */]();
articleCard.init();

// include
global.IncludeHTML = function (selector, filepath, callback) {
    $(function () {
        $.ajax({
            url: filepath,
            dataType: 'html',
            success: function success(data) {
                $(selector).html(data);

                // slider
                if (callback === 'blog_slider') {
                    var _slider2 = new __WEBPACK_IMPORTED_MODULE_4__modules_slider__["a" /* default */]();
                    _slider2.sliderTop();
                }

                // changeImg
                var changeImg = new __WEBPACK_IMPORTED_MODULE_2__modules_change_image__["a" /* default */]();
                changeImg.change(common.checkMedia());
                changeImg.change02(common.checkMedia());
                changeImg.changeBg(common.checkMedia());

                // slider
                if (callback === 'slider') {
                    var _slider3 = new __WEBPACK_IMPORTED_MODULE_4__modules_slider__["a" /* default */]();
                    _slider3.init();
                }

                // inc in inc
                if ($(selector).find('[id^="js-insert_"]').length) {
                    $(selector).find('[id^="js-insert_"]').each(function () {
                        var elm = $(this).attr('id');
                        var path = $(this).attr('data-include-path');
                        IncludeHTML('#' + elm, path);
                    });
                }
                // omitSentences
                common.omitSentences(common.checkMedia());
            }
        });
    });
};
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(1)))

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var accordion = function () {
    function accordion() {
        _classCallCheck(this, accordion);
    }

    _createClass(accordion, [{
        key: 'init',
        value: function init() {
            this.accordion();
            this.accordionMock();
        }
    }, {
        key: 'accordion',
        value: function accordion() {
            $(document).on('click touchend', '.js-accordion', function (e) {
                e.preventDefault();
                $(this).toggleClass('accordion-open');
                $(this).parent().toggleClass('is-active');
                $(this).next().slideToggle();
            });

            $(document).on('click touchend', '.js-accordion_close', function (e) {
                e.preventDefault();
                $(this).parent().slideUp();
                $(this).parent().prev().removeClass('accordion-open');
                $(this).parent().parent().removeClass('is-active');
            });
        }
    }, {
        key: 'spAccordion',
        value: function spAccordion(media) {
            if (media === 'sp') {
                $(document).on('click touchend', '.js-accordion_trigger', function (e) {
                    e.preventDefault();
                    $(this).toggleClass('accordion-open');
                    $(this).next().slideToggle();
                });

                $(document).on('click touchend', '.js-accordion_close', function (e) {
                    e.preventDefault();
                    $(this).parent().slideUp();
                    $(this).parent().prev().removeClass('accordion-open');
                });
            }
        }
    }, {
        key: 'spAccordion02',
        value: function spAccordion02(media) {
            if (media !== 'pc') {
                $(document).on('click touchend', '.js-header_nav', function (e) {
                    e.preventDefault();
                    $(this).toggleClass('accordion-open');
                    $(this).parent().toggleClass('is-active');
                    $(this).next().slideToggle();
                });

                $(document).on('click touchend', '.js-nav_close', function (e) {
                    e.preventDefault();
                    $(this).parent().parent().parent().slideUp();
                    $(this).parent().parent().parent().prev().removeClass('accordion-open');
                    $(this).parent().parent().parent().parent().removeClass('is-active');
                });
            }
        }
    }, {
        key: 'accordionNav',
        value: function accordionNav(media) {
            if (media !== 'pc') {
                $(document).on('click touchend', '.js-accordionNav_trigger', function (e) {
                    e.preventDefault();
                    $(this).toggleClass('accordion-open');
                    $(this).next().slideToggle();
                });

                $(document).on('click touchend', '.js-accordionNav_close', function (e) {
                    e.preventDefault();
                    $(this).parent().slideUp();
                    $(this).parent().prev().removeClass('accordion-open');
                });
            }
        }
    }, {
        key: 'accordionMock',
        value: function accordionMock() {
            $(document).on('click touchend', '.js-accordion-mock', function (e) {
                e.preventDefault();
                $(this).parent().parent().prev().slideDown();
            });
        }
    }]);

    return accordion;
}();

/* harmony default export */ __webpack_exports__["a"] = (accordion);

/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ChangeImage = function () {
    function ChangeImage() {
        _classCallCheck(this, ChangeImage);
    }

    _createClass(ChangeImage, [{
        key: 'change',
        value: function change(media) {
            var before = media !== 'sp' ? '_sp' : '_pc';
            var after = media === 'sp' ? '_sp' : '_pc';

            var myClass = document.getElementsByClassName('changeImg');
            for (var i = 0; i < myClass.length; i++) {
                var src = myClass[i].getAttribute('src');
                var newSrc = src.replace(before, after);
                myClass[i].setAttribute('src', newSrc);
            }
        }
    }, {
        key: 'change02',
        value: function change02(media) {
            var before = media === 'pc' ? '_sp' : '_pc';
            var after = media !== 'pc' ? '_sp' : '_pc';

            var myClass = document.getElementsByClassName('changeImg02');
            for (var i = 0; i < myClass.length; i++) {
                var src = myClass[i].getAttribute('src');
                var newSrc = src.replace(before, after);
                myClass[i].setAttribute('src', newSrc);
            }
        }
    }, {
        key: 'changeBg',
        value: function changeBg(media) {
            var $elm = $('.js-insert_bg');
            var str = '';
            $(window).on('load resize', function () {
                $elm.each(function () {
                    if (media !== 'sp') {
                        str = $(this).data('kv-pc');
                    } else {
                        str = $(this).data('kv-sp');
                    }
                    $(this).css('background-image', 'url(' + str + ')');
                });
            });
        }
    }]);

    return ChangeImage;
}();

/* harmony default export */ __webpack_exports__["a"] = (ChangeImage);

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var footer = function () {
    function footer() {
        _classCallCheck(this, footer);
    }

    _createClass(footer, [{
        key: 'init',
        value: function init() {
            this.logo_video();
        }
    }, {
        key: 'logo_video',
        value: function logo_video() {
            var video = $('#logo_video').get(0);
            $(function () {
                $('#logo_video').on('mouseenter , touchstart', function () {
                    video.play();
                });
            });
        }
    }]);

    return footer;
}();

/* harmony default export */ __webpack_exports__["a"] = (footer);

/***/ }),

/***/ 48:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var slider = function () {
    function slider() {
        _classCallCheck(this, slider);
    }

    _createClass(slider, [{
        key: 'init',
        value: function init() {
            this.sliderTop();
        }
    }, {
        key: 'sliderTop',
        value: function sliderTop() {
            $('.js-blogSlide').slick({
                speed: 1200,
                autoplaySpeed: 6000,
                autoplay: true,
                arrows: true,
                prevArrow: '<div class="blogSlide_prev"></div>',
                nextArrow: '<div class="blogSlide_next"></div>',
                dots: true,
                dotsClass: 'blogSlide_dots',
                pauseOnHover: false
            });
        }
    }]);

    return slider;
}();

/* harmony default export */ __webpack_exports__["a"] = (slider);

/***/ }),

/***/ 49:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__common__ = __webpack_require__(9);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var ArticleCard = function () {
    function ArticleCard() {
        _classCallCheck(this, ArticleCard);
    }

    _createClass(ArticleCard, [{
        key: 'init',
        value: function init() {
            var url = location.pathname;
            var _self = this;
            if (url == '/jp/blog/pr/') {
                var target = document.getElementById('js-insert_blog_article_pickup_pr');
            }
            if (url == '/jp/blog/creative/') {
                var target = document.getElementById('js-insert_blog_article_pickup_creative');
            }
            if (url == '/jp/blog/tech/') {
                var target = document.getElementById('js-insert_blog_article_pickup_tech');
            }
            if (url == '/jp/blog/tech/' || url == '/jp/blog/creative/' || url == '/jp/blog/pr/') {
                var observer = new MutationObserver(function (mutations) {
                    mutations.forEach(function (mutation) {
                        _self.triangleView();
                    });
                });
                observer.observe(target, {
                    childList: true,
                    characterData: true,
                    subtree: true
                });
                $(window).on('resize', function () {
                    _self.triangleView();
                });
            }
        }
    }, {
        key: 'triangleView',
        value: function triangleView() {
            var common = new __WEBPACK_IMPORTED_MODULE_0__common__["a" /* default */]();
            var media = common.checkMedia();
            var cardElm = $('.p-blog_articleCard-02.blog-main');
            var triangleElm = cardElm.find('.p-blog_articleCard_box_triangle');
            var height = void 0,
                width = void 0;

            if (media === 'sp') {
                width = $(window).width() * 0.93;
                height = $('.p-blog_articleCard_img').height() + (50 + 20) + $('.c-blog_heading-quaternary_wrap').height();
                var bottom = $('.p-blog_articleCard_box').outerHeight();

                triangleElm.css({
                    'border-bottom-width': height,
                    'border-left-width': width,
                    'bottom': bottom
                });
            } else {
                width = cardElm.width() * 0.63;
                height = cardElm.height();

                triangleElm.css({
                    'border-bottom-width': height,
                    'border-left-width': width,
                    'bottom': '-20px'
                });
            }
        }
    }]);

    return ArticleCard;
}();

/* harmony default export */ __webpack_exports__["a"] = (ArticleCard);

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var taglink = function () {
    function taglink() {
        _classCallCheck(this, taglink);
    }

    _createClass(taglink, [{
        key: 'init',
        value: function init() {
            this.taglink();
        }
    }, {
        key: 'taglink',
        value: function taglink() {
            $(document).on('click', '.c-js_tag_link', function (e) {
                e.stopPropagation();
                e.preventDefault();
                location.href = $(this).attr('data-url');
            });
        }
    }]);

    return taglink;
}();

/* harmony default export */ __webpack_exports__["a"] = (taglink);

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var link = function () {
    function link() {
        _classCallCheck(this, link);
    }

    _createClass(link, [{
        key: 'init',
        value: function init() {
            this.navLink();
        }
    }, {
        key: 'navLink',
        value: function navLink() {
            $('.c-blog_nav_item a').hover(function () {
                if (!$(this).hasClass('on-mouse')) {
                    var onMouse = $(this).children('img').attr('src').replace('off', 'on');
                    $(this).children('img').attr('src', onMouse);
                    $(this).addClass('on-mouse');
                } else {
                    var offMouse = $(this).children('img').attr('src').replace('on', 'off');
                    $(this).children('img').attr('src', offMouse);
                    $(this).removeClass('on-mouse');
                }
            });
        }
    }]);

    return link;
}();

/* harmony default export */ __webpack_exports__["a"] = (link);

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var matchHeight = function () {
    function matchHeight() {
        _classCallCheck(this, matchHeight);
    }

    _createClass(matchHeight, [{
        key: 'init',
        value: function init() {}
    }, {
        key: 'matchHeight',
        value: function matchHeight(media) {
            if (media !== 'sp') {
                $('.p-blog_card-02 .p-blog_card_txt').matchHeight();
                $('.p-blog_articleCard-03 .p-blog_articleCard_txt').matchHeight();
            }
        }
    }]);

    return matchHeight;
}();

/* harmony default export */ __webpack_exports__["a"] = (matchHeight);

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Nav = function () {
    function Nav() {
        _classCallCheck(this, Nav);
    }

    _createClass(Nav, [{
        key: 'init',
        value: function init() {}
    }, {
        key: 'mfSearch',
        value: function mfSearch() {
            $('.search-blog .search-blog_inner').prependTo('.mf-blogSearch');
        }
    }]);

    return Nav;
}();

/* unused harmony default export */ var _unused_webpack_default_export = (Nav);

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var url = location.pathname;
if (url == '/jp/blog/pr/' || url == '/jp/blog/creative/' || url == '/jp/blog/tech/') {
    var display_count = 11;
} else {
    var display_count = 10;
}
var load = false;
var moreBtn = '<div class="u-mt60-20 u-align-center"><div class="c-button_wrap"><div class="c-button-02-plus"><span>もっと見る</span></div></div></div>';

var output_json = function () {
    function output_json() {
        _classCallCheck(this, output_json);
    }

    _createClass(output_json, [{
        key: 'init',
        value: function init() {
            this.getJson();
            this.clickMore();
        }
    }, {
        key: 'getJson',
        value: function getJson() {
            if (!url.match('blog') || url.match('mt-preview')) return;
            this.getJsonBlog();
        }
    }, {
        key: 'getJsonBlog',
        value: function getJsonBlog() {
            var tag = [];
            var tag_text;
            var json = [];
            var _this = this;
            if (url.match("/jp/blog/pr/2") || url.match("/jp/blog/tech/2") || url.match("/jp/blog/creative/2")) {
                var article_primary = $('.l-blog_content-conn').data('primary');
                var category_primary = $('.l-blog_content-conn').data('category');
                tag.push(article_primary);
            }
            var url2 = location.href;
            var params = url2.split("?=");
            var spparams = params[1];
            if (url.match("/jp/blog/pr/2") || url.match("/jp/blog/tech/2") || url.match("/jp/blog/creative/2")) {

                var url_category = url.split("/");
                var jsonUrl = '/jp/blog/' + url_category[3] + '/assets/json/' + tag[0] + '.json';
                $.when($.getJSON(jsonUrl)).done(function (data) {
                    for (var i2 = 0; i2 < data.item.length; i2++) {
                        json.push(data.item[i2]);
                    }
                    _this.displayBlog(json, spparams);
                    _this.getJsonBlogSub(json, spparams);
                }).fail(function () {
                    $('.l-blog_content-conn').remove();
                    $('.l-blog_footer').addClass('u-mt80-40');
                });
            } else {
                var jsonPr = '/jp/blog/pr/assets/json/' + decodeURI(spparams) + '.json';
                var jsonCreative = '/jp/blog/creative/assets/json/' + decodeURI(spparams) + '.json';
                var jsonTech = '/jp/blog/tech/assets/json/' + decodeURI(spparams) + '.json';
            }
            if (url.match("/jp/blog/category/")) {
                $.when($.getJSON(jsonPr)).done(function (data) {
                    for (var i2 = 0; i2 < data.item.length; i2++) {
                        json.push(data.item[i2]);
                    }
                    _this.displayBlog(json, spparams);
                });
                $.when($.getJSON(jsonCreative)).done(function (data) {
                    for (var i2 = 0; i2 < data.item.length; i2++) {
                        json.push(data.item[i2]);
                    }
                    _this.displayBlog(json, spparams);
                });
                $.when($.getJSON(jsonTech)).done(function (data) {
                    for (var i2 = 0; i2 < data.item.length; i2++) {
                        json.push(data.item[i2]);
                    }
                    _this.displayBlog(json, spparams);
                });
            }
            if (url.match("/jp/blog/pr/category/")) {
                $.when($.getJSON(jsonPr)).done(function (data) {
                    for (var i2 = 0; i2 < data.item.length; i2++) {
                        json.push(data.item[i2]);
                    }
                    _this.displayBlog(json, spparams);
                });
            }
            if (url.match("/jp/blog/creative/category/")) {
                $.when($.getJSON(jsonCreative)).done(function (data) {
                    for (var i2 = 0; i2 < data.item.length; i2++) {
                        json.push(data.item[i2]);
                    }
                    _this.displayBlog(json, spparams);
                });
            }
            if (url.match("/jp/blog/tech/category/")) {
                $.when($.getJSON(jsonTech)).done(function (data) {
                    for (var i2 = 0; i2 < data.item.length; i2++) {
                        json.push(data.item[i2]);
                    }
                    _this.displayBlog(json, spparams);
                });
            }
            $(document).on('click', '.c-button-02-plus', function () {
                $('.c-button_wrap').parent('div').remove();
                display_count = display_count + 15;
                _this.displayBlog(json, spparams);
            });
        }
    }, {
        key: 'getJsonBlogSub',
        value: function getJsonBlogSub(json) {
            var _this2 = this;

            if (json.length < 4) {
                var json2;
                var tag2;
                var category_primary;
                var i;
                var url_category;
                var jsonUrl;

                (function () {
                    json2 = [];
                    tag2 = [];

                    var _this = _this2;
                    if (url.match("/jp/blog/pr/2") || url.match("/jp/blog/tech/2") || url.match("/jp/blog/creative/2")) {
                        category_primary = $('.l-blog_content-conn').data('category').split(',');

                        if (category_primary !== "") {
                            tag2.push(category_primary);
                        }
                    }
                    if ($('.l-blog_content-conn').data('category') !== "") {
                        for (i = 0; i < tag2[0].length; i++) {
                            if (url.match("/jp/blog/pr/2") || url.match("/jp/blog/tech/2") || url.match("/jp/blog/creative/2")) {
                                url_category = url.split("/");
                                jsonUrl = '/jp/blog/' + url_category[3] + '/assets/json/' + encodeURIComponent(tag2[0][i]) + '.json';
                            }
                            $.when($.getJSON(jsonUrl)).done(function (data) {
                                i = i - 1;
                                for (var i2 = 0; i2 < data.item.length; i2++) {
                                    json2.push(data.item[i2]);
                                    json2.sort(function (a, b) {
                                        if (a.date < b.date) return 1;
                                        if (a.date > b.date) return -1;
                                        return 0;
                                    });
                                }
                                if (i == 0) {
                                    _this.displayBlogSubHtml(json2);
                                }
                            }).fail(function () {
                                alert('読み込みに失敗しました。リロードしてください。');
                            });
                        }
                    }
                })();
            }
        }
    }, {
        key: 'displayBlogSubHtml',
        value: function displayBlogSubHtml(json) {
            var dataitem = json;
            display_count = 4 - $('.l-blog_grid article').length;
            if (display_count > json.length) {
                display_count = json.length;
            }
            var dataitem = json;
            var html = '';
            var urlArr = location.pathname.split('/');
            var i = 0;
            while (i < display_count) {
                var tag = dataitem[i].sub_type;
                var tagHtml = '';
                for (var j in tag) {
                    if (j >= tag.length) break;
                    tagHtml += '<span class="c-blog_tag">' + tag[j] + '</span>';
                }
                var linkTarget = dataitem[i].new_target;
                var linkClass = urlArr[urlArr.length - 2] === 'klabgames' && linkTarget === '_blank' ? 'c-games_link-win' : linkTarget === '_blank' ? 'c-games_list_link c-games_link-win' : 'c-games_link-arrow-02';
                var thum = dataitem[i].thumbnail_link ? dataitem[i].thumbnail_link : '/jp/assets/img/top/news03.png';
                var listClass = dataitem[i].link ? 'c-games_list_itemFigure' : 'c-games_list_itemFigure_noLink';
                var beforeDate = dataitem[i].format_date;
                beforeDate = beforeDate.split('/');
                var afterDate = beforeDate[0] + '-' + beforeDate[1] + '-' + beforeDate[2];
                if (i < 4) {
                    html += '<article class="p-blog_articleCard-01 l-blog_grid_4col">' + '<a href="' + dataitem[i].link + '"><div class="l-blog_grid_sp">' + '<div class="p-blog_articleCard_img"><img src="' + thum + '" width="283" height="159"' + 'alt="' + dataitem[i].title + '" class="changeImg">' + '</div><div class="p-blog_articleCard_box">' + '<h3 class="p-blog_articleCard_box_ttl">' + dataitem[i].title + '</h3><time datetime="' + afterDate + '">' + dataitem[i].format_date + '</time></div></div><ul class="c-blog_tag_wrap">' + tagHtml + '</ul></a></article>';
                }
                i++;
            }
            $('.l-blog_grid').append(html);
        }
    }, {
        key: 'displayBlog',
        value: function displayBlog(json, spparams) {
            var _this = this;
            var index = '-n + 4';
            var order = 'not(:nth-child(' + index + '))';
            json.sort(function (a, b) {
                if (a.date < b.date) return 1;
                if (a.date > b.date) return -1;
                return 0;
            });
            // if (spparams) {
            //     var filterCategory = json.filter(
            //     function(json){
            //       var dataCategory = json.sub_type;
            //       if (1 < dataCategory.length) {
            //           for(var i = 1; i<dataCategory.length; i++) {
            //             return dataCategory[i] == decodeURI(spparams)
            //           }
            //       }else {
            //         return dataCategory == decodeURI(spparams)
            //       }
            //     });
            //     json = filterCategory;
            // }
            if (json.length <= 10) {
                display_count = json.length;
            } else if (!url.match("/jp/blog/pr/2") && !url.match("/jp/blog/tech/2") && !url.match("/jp/blog/creative/2")) {
                $('.l-blog_contentSideNav_item').append(moreBtn);
            }
            if (json.length < display_count) {
                display_count = json.length;
                $('.c-button_wrap').parent('div').remove();
            }
            $(".p-blog_articleCard-01").remove();
            $(".p-blog_articleCard-02").remove();
            $('.l-blog_contentSideNav_item .l-blog_grid').remove();
            var target = $('.l-blog_contentSideNav_item');
            if (url.match("/jp/blog/pr/2") || url.match("/jp/blog/tech/2") || url.match("/jp/blog/creative/2")) {
                target = $('.l-blog_grid');
                var article_id = $('.l-blog_content-conn').data('article').toString();
                var mypage = json.filter(function (json) {
                    var dataGuid = json.guid;
                    return article_id !== dataGuid;
                });
                json = mypage;
                display_count = json.length;
                $(target).prepend(_this.createHtmlBlogPrimary(json, display_count));
            } else {
                $(target).prepend(_this.createHtmlBlog(json, display_count));
            }
            if (json.length >= display_count && load == false) {
                load = true;
            }
            $('.l-blog_contentSideNav_item article:' + order).wrapAll('<div class="l-blog_grid u-mt30-15">');
            $(function () {
                var count = 60;
                $('.l-blog_contentSideNav .p-blog_articleCard_box_txt').each(function () {
                    var thisText = $(this).text();
                    var textLength = thisText.length;
                    if (textLength > count) {
                        var showText = thisText.substring(0, count);
                        var insertText = showText += '…';
                        $(this).html(insertText);
                    };
                });
            });
            if (spparams) {
                $('.c-blog_heading').remove();
                $('.l-blog_contentSideNav_item').prepend('<h1 class="c-blog_heading c-blog_heading-undr u-mb30-20">' + decodeURI(spparams) + '</h1>');
            } else {
                $('.c-blog_heading').remove();
            }
        }
    }, {
        key: 'createHtmlBlog',
        value: function createHtmlBlog(json, display_count) {
            var dataitem = json;
            var html = '';
            var urlArr = location.pathname.split('/');
            var i = 0;
            while (i < display_count) {
                var tag = dataitem[i].sub_type;
                var tagHtml = '';
                for (var j in tag) {
                    if (j >= tag.length) break;
                    tagHtml += '<span class="c-blog_tag">' + tag[j] + '</span>';
                }
                var linkTarget = dataitem[i].new_target;
                var linkClass = urlArr[urlArr.length - 2] === 'klabgames' && linkTarget === '_blank' ? 'c-games_link-win' : linkTarget === '_blank' ? 'c-games_list_link c-games_link-win' : 'c-games_link-arrow-02';
                var thum = dataitem[i].thumbnail_link ? dataitem[i].thumbnail_link : '/jp/assets/img/top/news03.png';
                var listClass = dataitem[i].link ? 'c-games_list_itemFigure' : 'c-games_list_itemFigure_noLink';
                var beforeDate = dataitem[i].format_date;
                beforeDate = beforeDate.split('/');
                var afterDate = beforeDate[0] + '-' + beforeDate[1] + '-' + beforeDate[2];

                if (i < 4) {
                    html += '<article class="p-blog_articleCard-02"><a href="' + dataitem[i].link + '">' + '<div class="p-blog_articleCard_img"><img src="' + thum + '" width="424" height="239" alt="' + dataitem[i].title + '" class="changeImg"></div><div class="p-blog_articleCard_box">' + '<h3 class="p-blog_articleCard_box_ttl">' + dataitem[i].title + '</h3><time datetime="' + afterDate + '" class="u-mt10-10">' + dataitem[i].format_date + '</time>' + '<p class="p-blog_articleCard_box_txt">' + dataitem[i].text + '</p><ul class="u-mt10-5">' + tagHtml + '</ul></div></a></article>';
                } else {
                    html += '<article class="p-blog_articleCard-01 l-blog_grid_3col">' + '<a href="' + dataitem[i].link + '"><div class="l-blog_grid_sp">' + '<div class="p-blog_articleCard_img"><img src="' + thum + '" width="283" height="159"' + 'alt="' + dataitem[i].title + '" class="changeImg">' + '</div><div class="p-blog_articleCard_box">' + '<h3 class="p-blog_articleCard_box_ttl">' + dataitem[i].title + '</h3><time datetime="' + afterDate + '">' + dataitem[i].format_date + '</time></div></div><ul class="c-blog_tag_wrap">' + tagHtml + '</ul></a></article>';
                }
                i++;
            }
            return html;
        }
    }, {
        key: 'createHtmlBlogPrimary',
        value: function createHtmlBlogPrimary(json, display_count) {
            var json_none = $.isEmptyObject(json);
            if (json_none) {
                $('.l-blog_content-conn').remove();
            }
            var dataitem = json;
            var html = '';
            var urlArr = location.pathname.split('/');
            var i = 0;
            while (i < display_count) {
                var tag = dataitem[i].sub_type;
                var tagHtml = '';
                for (var j in tag) {
                    if (j >= tag.length) break;
                    tagHtml += '<span class="c-blog_tag">' + tag[j] + '</span>';
                }
                var linkTarget = dataitem[i].new_target;
                var linkClass = urlArr[urlArr.length - 2] === 'klabgames' && linkTarget === '_blank' ? 'c-games_link-win' : linkTarget === '_blank' ? 'c-games_list_link c-games_link-win' : 'c-games_link-arrow-02';
                var thum = dataitem[i].thumbnail_link ? dataitem[i].thumbnail_link : '/jp/assets/img/top/news03.png';
                var listClass = dataitem[i].link ? 'c-games_list_itemFigure' : 'c-games_list_itemFigure_noLink';
                var beforeDate = dataitem[i].format_date;
                beforeDate = beforeDate.split('/');
                var afterDate = beforeDate[0] + '-' + beforeDate[1] + '-' + beforeDate[2];
                if (i < 4) {
                    html += '<article class="p-blog_articleCard-01 l-blog_grid_4col">' + '<a href="' + dataitem[i].link + '"><div class="l-blog_grid_sp">' + '<div class="p-blog_articleCard_img"><img src="' + thum + '" width="283" height="159"' + 'alt="' + dataitem[i].title + '" class="changeImg">' + '</div><div class="p-blog_articleCard_box">' + '<h3 class="p-blog_articleCard_box_ttl">' + dataitem[i].title + '</h3><time datetime="' + afterDate + '">' + dataitem[i].format_date + '</time></div></div><ul class="c-blog_tag_wrap">' + tagHtml + '</ul></a></article>';
                }
                i++;
            }

            return html;
        }
    }, {
        key: 'clickMore',
        value: function clickMore() {
            var url = location.pathname.split('/');
            url = url[url.length - 2];
            var _this = this;
            if (location.pathname == '/jp/blog/') {
                var moreCount = 9;
                $('.l-blog_grid:nth-child(2) > .p-blog_articleCard-03:nth-child(n + ' + (moreCount + 1) + ')').addClass('is-hidden');
                _this.btnHidden();

                $(document).on('click touchend', '.c-blog_button-plus', function (event) {
                    $('.p-blog_articleCard-03.is-hidden').slice(0, moreCount + 6).removeClass('is-hidden');
                    _this.btnHidden();
                });
            }
            if (location.pathname == '/jp/blog/pr/' || location.pathname == '/jp/blog/creative/' || location.pathname == '/jp/blog/tech/') {
                var _moreCount = 12;
                $('.l-blog_grid > .p-blog_articleCard-01:nth-child(n + ' + (_moreCount + 1) + ')').addClass('is-hidden');
                _this.btnHidden();

                $(document).on('click touchend', '.c-blog_button-plus', function (event) {
                    $('.p-blog_articleCard-01.is-hidden').slice(0, _moreCount + 3).removeClass('is-hidden');
                    _this.btnHidden();
                });
            }
        }
    }, {
        key: 'btnHidden',
        value: function btnHidden() {
            if (location.pathname == '/jp/blog/') {
                if ($('.p-blog_articleCard-03.is-hidden').length == 0) {
                    $('.c-button_wrap').parent('div').remove();
                }
            }
            if (location.pathname == '/jp/blog/pr/' || location.pathname == '/jp/blog/creative/' || location.pathname == '/jp/blog/tech/') {
                if ($('.p-blog_articleCard-01.is-hidden').length == 0) {
                    $('.c-button_wrap').parent('div').remove();
                }
            }
        }
    }]);

    return output_json;
}();

/* harmony default export */ __webpack_exports__["a"] = (output_json);

/***/ }),

/***/ 9:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var common = function () {
    function common() {
        _classCallCheck(this, common);
    }

    _createClass(common, [{
        key: 'init',
        value: function init() {
            this.smoothScroll();
            this.objectFit();
            this.formSelect();
            this.viewportChange();
        }
    }, {
        key: 'smoothScroll',
        value: function smoothScroll() {
            //ページ内アンカーリンク以外のリンクはis_not_anchorlinkを付与してください。
            var anchor = $('a[href^="#"]').not('.is_not_anchorlink');
            $(document).on('click', 'a[href^="#"]', function (event) {
                if (!$(this).hasClass('is_not_anchorlink')) {
                    var href = $(this).attr('href'),
                        $target = $(href == '#' || href == "" ? 'html' : href),
                        position = $target.offset().top;
                    $('html, body').animate({ scrollTop: position }, 300, 'swing');
                    return false;
                }
            });

            //hashがあった場合の処理
            var hash = location.hash;
            if (location.hash !== '') {
                var $target = $(hash),
                    position = $target.offset().top;
                $('html, body').animate({ scrollTop: position }, 1, 'swing');
            }
        }
    }, {
        key: 'checkMedia',
        value: function checkMedia() {
            if (window.matchMedia('(max-width:767px)').matches) return 'sp';
            if (window.matchMedia('(min-width:1080px)').matches) return 'pc';
            return 'tab';
        }
    }, {
        key: 'objectFit',
        value: function objectFit() {
            objectFitImages();
        }
    }, {
        key: 'formSelect',
        value: function formSelect() {
            $('select').on('change', function () {
                $(this).addClass('empty');
            });
        }
    }, {
        key: 'omitSentences',
        value: function omitSentences(media) {
            $('.p-blog_articleCard-02 .p-blog_articleCard_box_ttl').each(function (index) {
                var text = $(this).text();
                var maxText = media !== 'sp' ? 82 : 86;
                var sliceText = text.length >= maxText ? text.slice(0, maxText) + '...' : text;
                $(this).text(sliceText);
            });
            $('.p-blog_articleCard-02 .p-blog_articleCard_box_txt').each(function (index) {
                var text = $(this).text();
                var sliceText = text.length >= 60 ? text.slice(0, 60) + '...' : text;
                $(this).text(sliceText);
            });
            $('.p-blog_articleCard-04 .p-blog_articleCard_txt').each(function (index) {
                var text = $(this).text();
                var maxText = media !== 'sp' ? 86 : 80;
                var sliceText = text.length >= maxText ? text.slice(0, maxText) + '...' : text;
                $(this).text(sliceText);
            });
            $('.p-blog_articleCard-03 .p-blog_articleCard_txt').each(function (index) {
                var text = $(this).text();
                var maxText = media !== 'sp' ? 80 : 47;
                var sliceText = text.length >= maxText ? text.slice(0, maxText) + '...' : text;
                $(this).text(sliceText);
            });
            $('.p-blog_card-02 .p-blog_card_txt').each(function (index) {
                var text = $(this).text();
                var maxText = media !== 'sp' ? 50 : 48;
                var sliceText = text.length >= maxText ? text.slice(0, maxText) + '...' : text;
                $(this).text(sliceText);
            });
        }
    }, {
        key: 'checkDevice',
        value: function checkDevice() {
            var userAgent = window.navigator.userAgent.toLowerCase();
            var appVersion = window.navigator.appVersion.toLowerCase();
            var device = void 0;
            if (userAgent.indexOf('ipad') > -1 || userAgent.indexOf('macintosh') > -1 && 'ontouchend' in document) {
                device = 'tab';
            } else if (userAgent.indexOf('iphone') > 0 || userAgent.indexOf('iPod') > 0 || userAgent.indexOf('android') > 0) {
                device = 'sp';
            } else {
                device = 'pc';
            }
            return device;
        }
    }, {
        key: 'viewportChange',
        value: function viewportChange() {
            var device = this.checkDevice();
            if (device === 'tab') {
                $("meta[name='viewport']").attr('content', 'width=1079');
            }
        }
    }]);

    return common;
}();

/* harmony default export */ __webpack_exports__["a"] = (common);

/***/ })

/******/ });
//# sourceMappingURL=script_blog.js.map