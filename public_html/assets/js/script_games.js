/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 30);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */,
/* 1 */
/***/ (function(module, exports) {

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var g;

// This works in non-strict mode
g = function () {
	return this;
}();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1, eval)("this");
} catch (e) {
	// This works if the window reference is available
	if ((typeof window === "undefined" ? "undefined" : _typeof(window)) === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;

/***/ }),
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var common = function () {
    function common() {
        _classCallCheck(this, common);
    }

    _createClass(common, [{
        key: 'init',
        value: function init() {
            this.smoothScroll();
            this.objectFit();
            this.formSelect();
            this.viewportChange();
        }
    }, {
        key: 'smoothScroll',
        value: function smoothScroll() {
            //ページ内アンカーリンク以外のリンクはis_not_anchorlinkを付与してください。
            var anchor = $('a[href^="#"]').not('.is_not_anchorlink');
            $(document).on('click', 'a[href^="#"]', function (event) {
                if (!$(this).hasClass('is_not_anchorlink')) {
                    var href = $(this).attr('href'),
                        $target = $(href == '#' || href == "" ? 'html' : href),
                        position = $target.offset().top;
                    $('html, body').animate({ scrollTop: position }, 300, 'swing');
                    return false;
                }
            });

            //hashがあった場合の処理
            var hash = location.hash;
            if (location.hash !== '') {
                var $target = $(hash),
                    position = $target.offset().top;
                $('html, body').animate({ scrollTop: position }, 1, 'swing');
            }
        }
    }, {
        key: 'checkMedia',
        value: function checkMedia() {
            if (window.matchMedia('(max-width:767px)').matches) return 'sp';
            if (window.matchMedia('(min-width:1080px)').matches) return 'pc';
            return 'tab';
        }
    }, {
        key: 'objectFit',
        value: function objectFit() {
            objectFitImages();
        }
    }, {
        key: 'formSelect',
        value: function formSelect() {
            $('select').on('change', function () {
                $(this).addClass('empty');
            });
        }
    }, {
        key: 'omitSentencesSp',
        value: function omitSentencesSp(media) {
            if (media === 'sp') {
                $('.omit-sp').each(function (index) {
                    var text = $(this).text();
                    var sliceText = text.length >= 47 ? text.slice(0, 47) + '...' : text;
                    $(this).text(sliceText);
                });
            }
        }
    }, {
        key: 'checkDevice',
        value: function checkDevice() {
            var userAgent = window.navigator.userAgent.toLowerCase();
            var appVersion = window.navigator.appVersion.toLowerCase();
            var device = void 0;
            if (userAgent.indexOf('ipad') > -1 || userAgent.indexOf('macintosh') > -1 && 'ontouchend' in document) {
                device = 'tab';
            } else if (userAgent.indexOf('iphone') > 0 || userAgent.indexOf('iPod') > 0 || userAgent.indexOf('android') > 0) {
                device = 'sp';
            } else {
                device = 'pc';
            }
            return device;
        }
    }, {
        key: 'viewportChange',
        value: function viewportChange() {
            var device = this.checkDevice();
            if (device === 'tab') {
                $("meta[name='viewport']").attr('content', 'width=1079');
            }
        }
    }]);

    return common;
}();

/* harmony default export */ __webpack_exports__["a"] = (common);

/***/ }),
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* WEBPACK VAR INJECTION */(function(global) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modules_common__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__modules_accordion__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modules_change_image__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modules_header__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modules_footer__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__modules_tab__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modules_slider__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modules_text__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__modules_sns__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__modules_link__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__modules_news__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__modules_match_height__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__modules_output_json__ = __webpack_require__(42);














// common
var common = new __WEBPACK_IMPORTED_MODULE_0__modules_common__["a" /* default */]();
common.init();
common.omitSentencesSp(common.checkMedia());

// accordion
var accordion = new __WEBPACK_IMPORTED_MODULE_1__modules_accordion__["a" /* default */]();
accordion.init();
accordion.spAccordion(common.checkMedia());
accordion.spAccordion02(common.checkMedia());
accordion.accordionNav(common.checkMedia());

// changeImg
var changeImg = new __WEBPACK_IMPORTED_MODULE_2__modules_change_image__["a" /* default */]();
changeImg.change(common.checkMedia());
changeImg.change02(common.checkMedia());
changeImg.changeBg(common.checkMedia());

// headeer
var header = new __WEBPACK_IMPORTED_MODULE_3__modules_header__["a" /* default */]();
header.init();
header.languageFade();
header.pcMegaMenu();

// footer
var footer = new __WEBPACK_IMPORTED_MODULE_4__modules_footer__["a" /* default */]();
footer.init();

// tab
var tab = new __WEBPACK_IMPORTED_MODULE_5__modules_tab__["a" /* default */]();
tab.init();

// slider
var slider = new __WEBPACK_IMPORTED_MODULE_6__modules_slider__["a" /* default */]();
slider.init();

// text
var text = new __WEBPACK_IMPORTED_MODULE_7__modules_text__["a" /* default */]();
text.init();

// sns
var sns = new __WEBPACK_IMPORTED_MODULE_8__modules_sns__["a" /* default */]();
sns.twitterEmbedded(common.checkMedia());

// link
var link = new __WEBPACK_IMPORTED_MODULE_9__modules_link__["a" /* default */]();
link.init();

// news
var news = new __WEBPACK_IMPORTED_MODULE_10__modules_news__["a" /* default */]();
news.init();

// matchHeight
var matchHeight = new __WEBPACK_IMPORTED_MODULE_11__modules_match_height__["a" /* default */]();
matchHeight.matchHeight(common.checkMedia());

// output_json
var output_json = new __WEBPACK_IMPORTED_MODULE_12__modules_output_json__["a" /* default */]();
$(window).on("load", function () {
    output_json.init();
});

// include
global.IncludeHTML = function (selector, filepath, callback) {
    $(function () {
        $.ajax({
            url: filepath,
            dataType: 'html',
            success: function success(data) {
                $(selector).html(data);

                // changeImg
                var changeImg = new __WEBPACK_IMPORTED_MODULE_2__modules_change_image__["a" /* default */]();
                changeImg.changeBg(common.checkMedia());
                $(window).on("load resize", function () {
                    changeImg.change(common.checkMedia());
                    changeImg.change02(common.checkMedia());
                });

                // slider
                if (callback === 'slider') {
                    var _slider2 = new __WEBPACK_IMPORTED_MODULE_6__modules_slider__["a" /* default */]();
                    _slider2.sliderKv();
                }

                // mfSearch
                header.mfSearch();

                // inc in inc
                if ($(selector).find('[id^="js-insert_"]').length) {
                    $(selector).find('[id^="js-insert_"]').each(function () {
                        var elm = $(this).attr('id');
                        var path = $(this).attr('data-include-path');
                        IncludeHTML('#' + elm, path);
                    });
                }
            }
        });
    });
};
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(1)))

/***/ }),
/* 31 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var accordion = function () {
    function accordion() {
        _classCallCheck(this, accordion);
    }

    _createClass(accordion, [{
        key: 'init',
        value: function init() {
            this.accordion();
            this.accordionMock();
        }
    }, {
        key: 'accordion',
        value: function accordion() {
            $(document).on('click touchend', '.js-accordion', function (e) {
                e.preventDefault();
                $(this).toggleClass('accordion-open');
                $(this).parent().toggleClass('is-active');
                $(this).next().slideToggle();
            });

            $(document).on('click touchend', '.js-accordion_close', function (e) {
                e.preventDefault();
                $(this).parent().slideUp();
                $(this).parent().prev().removeClass('accordion-open');
                $(this).parent().parent().removeClass('is-active');
            });
        }
    }, {
        key: 'spAccordion',
        value: function spAccordion(media) {
            if (media === 'sp') {
                $(document).on('click touchend', '.js-accordion_trigger', function (e) {
                    e.preventDefault();
                    $(this).toggleClass('accordion-open');
                    $(this).next().slideToggle();
                });

                $(document).on('click touchend', '.js-accordion_close', function (e) {
                    e.preventDefault();
                    $(this).parent().slideUp();
                    $(this).parent().prev().removeClass('accordion-open');
                });
            }
        }
    }, {
        key: 'spAccordion02',
        value: function spAccordion02(media) {
            if (media !== 'pc') {
                $(document).on('click touchend', '.js-header_nav', function (e) {
                    e.preventDefault();
                    $(this).toggleClass('accordion-open');
                    $(this).parent().toggleClass('is-active');
                    $(this).next().slideToggle();
                });

                $(document).on('click touchend', '.js-nav_close', function (e) {
                    e.preventDefault();
                    $(this).parent().parent().parent().slideUp();
                    $(this).parent().parent().parent().prev().removeClass('accordion-open');
                    $(this).parent().parent().parent().parent().removeClass('is-active');
                });
            }
        }
    }, {
        key: 'accordionNav',
        value: function accordionNav(media) {
            if (media !== 'pc') {
                $(document).on('click touchend', '.js-accordionNav_trigger', function (e) {
                    e.preventDefault();
                    $(this).toggleClass('accordion-open');
                    $(this).next().slideToggle();
                });

                $(document).on('click touchend', '.js-accordionNav_close', function (e) {
                    e.preventDefault();
                    $(this).parent().slideUp();
                    $(this).parent().prev().removeClass('accordion-open');
                });
            }
        }
    }, {
        key: 'accordionMock',
        value: function accordionMock() {
            $(document).on('click touchend', '.js-accordion-mock', function (e) {
                e.preventDefault();
                $(this).parent().parent().prev().slideDown();
            });
        }
    }]);

    return accordion;
}();

/* harmony default export */ __webpack_exports__["a"] = (accordion);

/***/ }),
/* 32 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ChangeImage = function () {
    function ChangeImage() {
        _classCallCheck(this, ChangeImage);
    }

    _createClass(ChangeImage, [{
        key: 'change',
        value: function change(media) {
            $(window).on('load resize', function () {
                var before = media !== 'sp' ? '_sp' : '_pc';
                var after = media === 'sp' ? '_sp' : '_pc';

                var myClass = document.getElementsByClassName('changeImg');
                for (var i = 0; i < myClass.length; i++) {
                    var src = myClass[i].getAttribute('src');
                    var newSrc = src.replace(before, after);
                    myClass[i].setAttribute('src', newSrc);
                }
            });
        }
    }, {
        key: 'change02',
        value: function change02(media) {
            $(window).on('load resize', function () {
                var before = media === 'pc' ? '_sp' : '_pc';
                var after = media !== 'pc' ? '_sp' : '_pc';

                var myClass = document.getElementsByClassName('changeImg02');
                for (var i = 0; i < myClass.length; i++) {
                    var src = myClass[i].getAttribute('src');
                    var newSrc = src.replace(before, after);
                    myClass[i].setAttribute('src', newSrc);
                }
            });
        }
    }, {
        key: 'changeBg',
        value: function changeBg(media) {
            var $elm = $('.js-insert_bg');
            var str = '';
            $(window).on('load resize', function () {
                $elm.each(function () {
                    if (media !== 'sp') {
                        str = $(this).data('kv-pc');
                    } else {
                        str = $(this).data('kv-sp');
                    }
                    $(this).css('background-image', 'url(' + str + ')');
                });
            });
        }
    }]);

    return ChangeImage;
}();

/* harmony default export */ __webpack_exports__["a"] = (ChangeImage);

/***/ }),
/* 33 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__common__ = __webpack_require__(8);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var header = function () {
    function header() {
        _classCallCheck(this, header);
    }

    _createClass(header, [{
        key: 'init',
        value: function init() {
            this.menuOpen();
            this.headerSearch();
            this.resizeHeader();

            var _self = this;

            var isScroll = false;
            var timeoutId = void 0;

            $(window).scroll(function () {
                isScroll = true;
                _self.fixUp();

                clearTimeout(timeoutId);
                timeoutId = setTimeout(function () {
                    isScroll = false;
                    if ($('.l-games_header').hasClass('hide') && !isScroll) {
                        $('.l-games_header').removeClass('hide');
                    }
                }, 5000);
            });

            $(window).on('load', function () {
                _self.startPos = 0;
                _self.winScrollTop = 0;
                $('.l-games_header').removeClass('hide');
            });
        }
    }, {
        key: 'menuOpen',
        value: function menuOpen() {
            var scrollTop = void 0;
            var _self = this;

            $(document).on('click touchend', '.js-open_trigger', function (e) {
                e.preventDefault();
                menu_toggle();
                $('.js-search').toggleClass('is-active');
            });

            $(document).on('click touchend', '.l-games_header_inner', function (e) {
                // e.preventDefault();
                if ($(window).width() < 1080) {
                    menu_toggle();
                    $('.js-search').toggleClass('is-active');
                }
            });

            $(document).on('click touchend', '.l-games_header_btn_list', function (e) {
                e.stopPropagation();
            });
            $(document).on('click touchend', '.l-games_header_nav_wrap', function (e) {
                e.stopPropagation();
            });
            $(document).on('click touchend', '.l-games_header_list', function (e) {
                e.stopPropagation();
            });

            function menu_toggle() {
                $('.js-open_trigger').toggleClass('is-open');
                $('.js-open_toggle').slideToggle(300);
                if ($('.js-open_trigger').hasClass('is-open')) {
                    scrollTop = $(window).scrollTop();
                    $('html, body').toggleClass('is-fixed').css('top', -scrollTop);
                } else {
                    var _Date = function _Date() {
                        $('html, body').removeClass('is-fixed').css('top', 'auto');
                        $(window).scrollTop(scrollTop);
                    };

                    var promise = new Promise(function (resolve) {
                        resolve(new _Date());
                    });
                    promise.then(function (data) {
                        setTimeout(function () {
                            $('.l-header').removeClass('hide');
                        }, 10);
                    });
                }
            }
        }
    }, {
        key: 'languageFade',
        value: function languageFade() {
            $(document).on({
                'mouseenter': function mouseenter() {
                    if (window.matchMedia('(min-width:1080px)').matches) {
                        $(this).stop(false, false).addClass('is-open');
                    }
                },
                'mouseleave': function mouseleave() {
                    if (window.matchMedia('(min-width:1080px)').matches) {
                        $(this).stop(false, false).removeClass('is-open');
                    }
                }
            }, '.js-languagen_trigger');
        }
    }, {
        key: 'pcMegaMenu',
        value: function pcMegaMenu() {
            $(document).on({
                'mouseenter': function mouseenter() {
                    if (window.matchMedia('(min-width:1080px)').matches) {
                        $(this).children('.l-games_header_megaMenu').stop(false, false).slideDown(300);
                    }
                },
                'mouseleave': function mouseleave() {
                    if (window.matchMedia('(min-width:1080px)').matches) {
                        $(this).children('.l-games_header_megaMenu').stop(false, false).slideUp(300);
                    }
                }
            }, '.js-menu_trigge');
        }
    }, {
        key: 'fixUp',
        value: function fixUp() {
            var _self = this;
            var doch = $(document).innerHeight();
            var winh = $(window).innerHeight();
            var bottom = doch - winh;
            _self.winScrollTop = $(window).scrollTop();
            if (_self.winScrollTop <= 0 || _self.startPos > _self.winScrollTop || _self.winScrollTop >= bottom - 20) {
                $('.l-games_header').removeClass('hide');
            } else {
                $('.l-games_header').addClass('hide');
            }
            _self.startPos = _self.winScrollTop;
        }
    }, {
        key: 'headerSearch',
        value: function headerSearch() {
            $(window).on('load resize', function () {
                $('.mf_finder_searchBox_submit').on('click', function () {
                    $('input[name="imgsize"]').attr('value', 3);
                });
                var w = $(window).width();
                var x = 1080;
                if (w >= x) {
                    $('.js-search').removeClass('js-click');
                    $('.js-search').addClass('search_off');
                    $(document).on('click touchend', '.search_off', function (e) {
                        e.preventDefault();
                        $('.js-search img').attr('src', '/assets/img/icon/search_close.png');
                        $('.js-search').removeClass('search_off');
                        $('.js-search').addClass('search_on');
                        $('.js-searchArea').stop(false, false).slideDown(300);
                    });
                    $(document).on('click touchend', '.search_on', function (e) {
                        e.preventDefault();
                        $('.js-search img').attr('src', '/assets/img/icon/search.png');
                        $('.js-search').removeClass('search_on');
                        $('.js-search').addClass('search_off');
                        $('.js-searchArea').stop(false, false).slideUp(300);
                        $('.js-searchArea').css('height', 'auto');
                    });
                    $(document).on('click touchend', '.js-searchArea', function (e) {
                        e.stopPropagation();
                    });
                } else {
                    $('.js-search img').attr('src', '/assets/img/icon/search.png');
                    $('.js-search').removeClass('search_off');
                    $('.js-search').removeClass('search_on');
                    $('.js-search').removeClass('js-hover');
                    $('.js-search').addClass('js-click');
                    $('.js-searchArea').css('height', '');
                    $(document).on('click touchend', '.js-click', function (e) {
                        e.preventDefault();
                        $('.js-searchArea').css('height', 'calc(100% - 48px)');
                        $('.js-searchArea').slideDown(300);
                        $('.js-open_trigger').addClass('is-active');
                        $('.l-games_header_search_trigger').addClass('is-open');
                    });

                    $(document).on('click touchend', '.l-games_header_search_trigger', function (e) {
                        e.preventDefault();
                        $('.js-searchArea').css('height', 'auto');
                        $('.js-searchArea').slideUp(300);
                        $('.js-open_trigger').removeClass('is-active');
                        $(this).removeClass('is-open');
                    });

                    $(document).on('click touchend', '.js-searchArea', function (e) {
                        e.preventDefault();
                        $('.js-searchArea').slideUp(300);
                        $('.js-searchArea').css('height', '');
                        $('.js-open_trigger').removeClass('is-active');
                        $('.l-games_header_search_trigger').removeClass('is-open');
                    });

                    $(document).on('click touchend', '.l-games_header_searchArea_inner', function (e) {
                        e.stopPropagation();
                    });
                }
            });
        }
    }, {
        key: 'mfSearch',
        value: function mfSearch() {
            var initialW = void 0;
            var initialMedia = void 0;

            $(window).on('load', function () {
                initialW = $(window).width();
                initialMedia = initialW >= 1080 ? 'pc' : 'sp';

                if (initialW >= 1080) {
                    $('.search-header').prependTo('.search-pc');
                    $('.search-header').css('display', 'block');
                } else {
                    $('.search-header').prependTo('.search-sp');
                    $('.search-header').css('display', 'block');
                }
            });

            $(window).on('resize', function () {
                var carrentW = $(window).width();
                var carrentMedia = carrentW >= 1080 ? 'pc' : 'sp';

                if (carrentMedia !== initialMedia) {
                    initialMedia = carrentMedia;
                    if (carrentW >= 1080) {
                        $('.search-header').prependTo('.search-pc');
                        $('.search-header').css('display', 'block');
                    } else {
                        $('.search-header').prependTo('.search-sp');
                        $('.search-header').css('display', 'block');
                    }
                }
            });
        }
    }, {
        key: 'resizeHeader',
        value: function resizeHeader() {
            var initialW = void 0;
            var initialMedia = void 0;
            $(window).on('load', function () {
                initialW = $(window).width();
                initialMedia = initialW >= 1080 ? 'pc' : 'sp';
            });

            $(window).on('resize', function () {
                var carrentW = $(window).width();
                var carrentMedia = carrentW >= 1080 ? 'pc' : 'sp';

                if (carrentMedia !== initialMedia) {
                    initialMedia = carrentMedia;
                    if (carrentW < 1080) {
                        $('html').removeClass('is-fixed');
                        $('body').removeClass('is-fixed');
                        $('.js-open_toggle').css('display', 'none');
                        $('.js-open_trigger').removeClass('is-open');
                        $('.js-open_trigger').removeClass('is-active');
                        $('.js-search').removeClass('is-active');
                        $('.l-games_header_search_trigger').removeClass('is-open');
                        $('.js-searchArea').css('display', 'none');
                    } else {
                        $('.js-open_toggle').css('display', 'flex');
                    }
                }
            });
        }
    }]);

    return header;
}();

/* harmony default export */ __webpack_exports__["a"] = (header);

/***/ }),
/* 34 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var footer = function () {
    function footer() {
        _classCallCheck(this, footer);
    }

    _createClass(footer, [{
        key: 'init',
        value: function init() {
            this.logo_video();
        }
    }, {
        key: 'logo_video',
        value: function logo_video() {
            var video = $('#logo_video').get(0);
            $(function () {
                $('#logo_video').on('mouseenter , touchstart', function () {
                    video.play();
                });
            });
        }
    }]);

    return footer;
}();

/* harmony default export */ __webpack_exports__["a"] = (footer);

/***/ }),
/* 35 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var tab = function () {
    function tab() {
        _classCallCheck(this, tab);
    }

    _createClass(tab, [{
        key: 'init',
        value: function init() {
            this.tabSwitching();
        }
    }, {
        key: 'tabSwitching',
        value: function tabSwitching() {
            $(document).on('click', '.js-tab_item', function () {
                $('.is-active').removeClass('is-active');
                $(this).addClass('is-active');
                $('.is-show').removeClass('is-show');
                var index = $(this).index();
                $('.js-tab_panel').eq(index).addClass('is-show');
            });
        }
    }]);

    return tab;
}();

/* harmony default export */ __webpack_exports__["a"] = (tab);

/***/ }),
/* 36 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var slider = function () {
    function slider() {
        _classCallCheck(this, slider);
    }

    _createClass(slider, [{
        key: 'init',
        value: function init() {
            this.sliderKv();
            this.articleSlide();
            this.gamesSlick();
        }
    }, {
        key: 'sliderKv',
        value: function sliderKv() {
            $('.js-games-kv').on('init', function (event, slick) {
                var slideNum = slick.slideCount;
                var html = '';
                var circle = '\n                <svg class="p-games_top_hero_nav_svg" width="20" height="20">\n                    <circle cx="10" cy="10" r="8" class="p-games_top_hero_nav_circle1" />\n                    <circle cx="10" cy="10" r="8" class="p-games_top_hero_nav_circle2" />\n                </svg>\n                ';

                for (var i = 0; slideNum > i; i++) {
                    var selector = '.p-games_top_hero .p-games_top_hero_item[data-slick-index="' + i + '"]';
                    var thumb = $(selector).data('kv-thum');

                    if (i === 0) {
                        html += '<div class="p-games_top_hero_nav_item is-active">';
                    } else {
                        html += '<div class="p-games_top_hero_nav_item">';
                    }

                    html += '\n                            <img src="' + thumb + '" alt="" class="p-games_top_hero_nav_thumb">\n                            ' + circle + '\n                        </div>\n                    ';
                }

                $('.p-games_top_hero_nav').append(html);
            }).slick({
                speed: 1200,
                autoplaySpeed: 6000,
                autoplay: true,
                arrows: false,
                draggable: false,
                pauseOnHover: false
            }).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $('.p-games_top_hero_nav_item').removeClass('is-active');
                $('.p-games_top_hero_nav_item').eq(nextSlide).addClass('is-active');

                cicle = $('.is-active .p-games_top_hero_nav_circle2');
                cicle_count = 0;
                cicle.css({ 'stroke-dasharray': '0 51' });
            });
            var cicle_count = 9;
            var cicle = $('.is-active .p-games_top_hero_nav_circle2');
            var countup = function countup() {
                if (cicle_count <= 51) {
                    cicle.css({ 'stroke-dasharray': cicle_count + ' 51' });
                    cicle_count = cicle_count + 0.25;
                } else {
                    cicle_count = 0;
                }
            };
            setInterval(countup, 35.5);

            // click circle
            $(document).on('click', '.p-games_top_hero_nav_item', function () {
                var index = $(this).index();
                $('.js-games-kv').slick('slickGoTo', index);
            });
        }
    }, {
        key: 'articleSlide',
        value: function articleSlide() {
            $('.js-articleSlide').slick({
                speed: 1200,
                autoplaySpeed: 6000,
                autoplay: true,
                arrows: true,
                slidesToShow: 3,
                prevArrow: '<div class="articleSlide_prev"></div>',
                nextArrow: '<div class="articleSlide_next"></div>',
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1
                    }
                }]
            });
        }
    }, {
        key: 'gamesSlick',
        value: function gamesSlick() {
            var _$$slick;

            $('.js-slick-games').slick((_$$slick = {
                speed: 1200,
                autoplaySpeed: 6000,
                // autoplay: true,
                arrows: true,
                pauseOnHover: false,
                variableWidth: true,
                centerMode: true,
                centerPadding: '5.55553vw',
                slidesToShow: 5,
                dots: true,
                dotsClass: 'banner_dots'
            }, _defineProperty(_$$slick, 'arrows', true), _defineProperty(_$$slick, 'prevArrow', '<div class="kv_prev"></div>'), _defineProperty(_$$slick, 'nextArrow', '<div class="kv_next"></div>'), _defineProperty(_$$slick, 'responsive', [{
                breakpoint: 767,
                settings: {
                    slidesToShow: 1
                }
            }]), _$$slick));
        }
    }]);

    return slider;
}();

/* harmony default export */ __webpack_exports__["a"] = (slider);

/***/ }),
/* 37 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var text = function () {
    function text() {
        _classCallCheck(this, text);
    }

    _createClass(text, [{
        key: "init",
        value: function init() {
            this.textSpan();
        }
    }, {
        key: "textSpan",
        value: function textSpan() {
            $(".text_span").children().addBack().contents().each(function () {
                if (this.nodeType == 3) {
                    $(this).replaceWith($(this).text().replace(/(\S)/g, '<span class="title_bg"><span class="title_text">$&</span></span>'));
                }
            });
        }
    }]);

    return text;
}();

/* harmony default export */ __webpack_exports__["a"] = (text);

/***/ }),
/* 38 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var sns = function () {
    function sns() {
        _classCallCheck(this, sns);
    }

    _createClass(sns, [{
        key: 'init',
        value: function init() {}
    }, {
        key: 'twitterEmbedded',
        value: function twitterEmbedded(media) {
            $(window).on('load resize', function () {
                if (media !== 'sp') {
                    var elementHeight = $('.js-height').height();
                    var subtractionHeight = $('.js-subtraction').height();
                    var formula = elementHeight - subtractionHeight;
                    $('.js-embedded').height(formula);
                }
            });
        }
    }]);

    return sns;
}();

/* harmony default export */ __webpack_exports__["a"] = (sns);

/***/ }),
/* 39 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var link = function () {
    function link() {
        _classCallCheck(this, link);
    }

    _createClass(link, [{
        key: 'init',
        value: function init() {
            this.articleList();
        }
    }, {
        key: 'articleList',
        value: function articleList() {
            $(document).on({
                'mouseenter': function mouseenter() {
                    $(this).parent().prev().children().addClass('is-hover');
                },
                'mouseleave': function mouseleave() {
                    $(this).parent().prev().children().removeClass('is-hover');
                }
            }, '.games-link');

            $(document).on({
                'mouseenter': function mouseenter() {
                    $(this).parent().next().children().addClass('is-hover');
                },
                'mouseleave': function mouseleave() {
                    $(this).parent().next().children().removeClass('is-hover');
                }
            }, '.games-figure');
        }
    }]);

    return link;
}();

/* harmony default export */ __webpack_exports__["a"] = (link);

/***/ }),
/* 40 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var news = function () {
    function news() {
        _classCallCheck(this, news);
    }

    _createClass(news, [{
        key: 'init',
        value: function init() {
            this.news_fixed();
        }
    }, {
        key: 'news_fixed',
        value: function news_fixed() {
            $(document).on('click', '.p-games_top_news_close', function (event) {
                $('.p-games_top_news').toggleClass('active');
                $('.p-games_top_news_trigger').toggleClass('active');
            });

            $(document).on('click', '.p-games_top_news_trigger', function () {
                $('.p-games_top_news').toggleClass('active');
                $('.p-games_top_news_trigger').toggleClass('active');
            });
        }
    }]);

    return news;
}();

/* harmony default export */ __webpack_exports__["a"] = (news);

/***/ }),
/* 41 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var matchHeight = function () {
    function matchHeight() {
        _classCallCheck(this, matchHeight);
    }

    _createClass(matchHeight, [{
        key: 'init',
        value: function init() {}
    }, {
        key: 'matchHeight',
        value: function matchHeight(media) {
            if (media !== 'sp') {
                $('.p-games_top_articleCard .p-games_top_articleCard_box').matchHeight();
            }
        }
    }]);

    return matchHeight;
}();

/* harmony default export */ __webpack_exports__["a"] = (matchHeight);

/***/ }),
/* 42 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var checkData = null;
var l2 = 15;

var output_json = function () {
  function output_json() {
    _classCallCheck(this, output_json);
  }

  _createClass(output_json, [{
    key: 'init',
    value: function init() {
      this.getJson();
    }
  }, {
    key: 'getJson',
    value: function getJson() {
      var url = location.pathname;
      if (!url.match('klabgames') || url.match('mt-preview')) return;
      this.getJsonGames();
    }
  }, {
    key: 'getJsonGames',
    value: function getJsonGames() {
      var _this = this;
      var path = window.location.pathname;
      var lang = path.match(/jp/) ? '/jp/' : '/en/';
      $.when($.getJSON(lang + 'assets/inc/json/games.json'), $.getJSON(lang + 'assets/inc/json/games_news.json'), $.getJSON(lang + 'assets/inc/json/games_news_top.json'), $.getJSON(lang + 'assets/inc/json/games_top.json'), $.getJSON(lang + 'assets/inc/json/games_news_topics.json'), $.getJSON(lang + 'assets/inc/json/games_topics.json')
      // $.getJSON(lang + 'assets/inc/json/games2014.json'),
      // $.getJSON(lang + 'assets/inc/json/games2015.json'),
      // $.getJSON(lang + 'assets/inc/json/games2016.json'),
      // $.getJSON(lang + 'assets/inc/json/games2017.json'),
      // $.getJSON(lang + 'assets/inc/json/games2018.json'),
      // $.getJSON(lang + 'assets/inc/json/games2019.json'),
      // $.getJSON(lang + 'assets/inc/json/games2020.json')
      ).done(function (games, games_news, games_top, games_top_news, games_news_topics, games_topics) {
        var urlArr = location.pathname.split('/');
        // klabgames top
        if (urlArr[urlArr.length - 2] === 'klabgames') {
          _this.displayGamesTop(games_top, games_top_news);

          // klabgames topics
        } else if (urlArr[urlArr.length - 2] === 'topics') {
          _this.displayGamesTopics(games_news_topics, games_topics);
        } else if (urlArr[urlArr.length - 3] === 'titles') {
          _this.displayGamesTitle(games_news_topics, games_topics);
        }
      }).fail(function () {
        alert('読み込みに失敗しました。リロードしてください。');
      });
    }
  }, {
    key: 'displayGamesTitle',
    value: function displayGamesTitle(data_a, data_b) {
      var json1 = data_a[0].item;
      var json2 = data_b[0].item;
      // jsonをマージ
      var json3 = json1.concat(json2);
      // 日付順にソート
      json3.sort(function (a, b) {
        if (a.date < b.date) return 1;
        if (a.date > b.date) return -1;
        return 0;
      });
      //フィルター処理
      var urlArr = location.pathname.split('/');
      urlArr = urlArr[urlArr.length - 2];
      var a = json3.length;
      var filterCategory = json3.filter(function (index) {
        return index.type == urlArr;
      });
      var _this = this;
      var target = $('#js-insert_games_topics .c-games_listTime-02');
      // 初期表示数
      var l = 3;
      target.html('');
      $(target).append(_this.createHtmlGamesTitle(filterCategory, l));
    }
  }, {
    key: 'createHtmlGamesTitle',
    value: function createHtmlGamesTitle(data, l) {
      var urlArr = location.pathname.split('/');
      var html = '';
      var dataitem = data;
      if (dataitem.length < l) {
        l = dataitem.length;
      } else {
        l = Number(l);
      }
      for (var i = 0; i < l; i++) {
        var linkTarget = dataitem[i].new_target;
        var linkClass = urlArr[urlArr.length - 2] === 'klabgames' && linkTarget === '_blank' ? 'c-games_link-win' : linkTarget === '_blank' ? 'c-games_list_link c-games_link-win' : 'c-games_list_link';
        var listClass = dataitem[i].link ? '' : 'no-link';
        var beforeDate = dataitem[i].format_date;
        beforeDate = beforeDate.split('/');
        var afterDate = beforeDate[0] + '-' + beforeDate[1] + '-' + beforeDate[2];
        html += '<li class="' + listClass + '">' + '<a href="' + dataitem[i].link + '" target="' + linkTarget + '">' + '<time datetime="' + afterDate + '">' + dataitem[i].format_date + '</time>' + '<span class="' + linkClass + '">' + dataitem[i].title + '</span>' + '</a>' + '</li>';
      }
      return html;
    }
  }, {
    key: 'displayGamesTop',
    value: function displayGamesTop(data_a, data_b) {
      var json1 = data_a[0].item;
      var json2 = data_b[0].item;
      // jsonをマージ
      var json3 = json1.concat(json2);
      // 日付順にソート
      json3.sort(function (a, b) {
        if (a.date < b.date) return 1;
        if (a.date > b.date) return -1;
        return 0;
      });

      var _this = this;
      var target = $('.c-games_listTime-03');
      // 初期表示数
      var l = 5;
      target.html('');
      $(target).append(_this.createHtmlGames(json3, l));
    }
  }, {
    key: 'createHtmlGames',
    value: function createHtmlGames(data, l) {
      var urlArr = location.pathname.split('/');
      var html = '';
      var dataitem = data;
      l = Number(l);
      var quantity = urlArr[urlArr.length - 2] === 'klabgames' ? 5 : 15;

      for (var i = l - quantity; i < l; i++) {
        if (i >= dataitem.length) {
          $('.c-button_wrap').parent('div').remove();
          break;
        }

        var tag = dataitem[i].sub_type;
        var tagHtml = '';
        var tagarr = {
          'UPDATE': 'c-games_tag',
          'EVENTS': 'c-games_tag-02',
          'CAMPAIGN': 'c-games_tag-03',
          'INFORMATION': 'c-games_tag-04',
          'WEB': 'c-tag',
          'イベント': 'c-tag-02',
          '雑誌': 'c-tag-03',
          '新聞': 'c-tag-04',
          'テレビ': 'c-tag-05'
        };
        for (var j in tag) {
          if (j >= tag.length) break;
          tagHtml += '<span class="' + tagarr[tag[j].toUpperCase()] + '">' + tag[j] + '</span>';
        }

        var linkTarget = dataitem[i].new_target;
        var linkClass = urlArr[urlArr.length - 2] === 'klabgames' && linkTarget === '_blank' ? 'c-games_link-win' : linkTarget === '_blank' ? 'c-games_list_link c-games_link-win' : 'c-games_link-arrow-02 c-games_link-arrow-pc';
        var thum = dataitem[i].thumbnail_link ? dataitem[i].thumbnail_link : '/jp/assets/img/top/news03.png';
        var listClass = dataitem[i].link ? 'c-games_list_itemFigure' : 'c-games_list_itemFigure_noLink';
        var beforeDate = dataitem[i].format_date;
        beforeDate = beforeDate.split('/');
        var afterDate = beforeDate[0] + '-' + beforeDate[1] + '-' + beforeDate[2];

        html += '<li class="' + listClass + '">' + '<a href="' + dataitem[i].link + '" target="' + linkTarget + '">' + '<div class="c-games_list_figure">' + '<img src="' + thum + '" alt="' + dataitem[i].title + '" width="156" height="156">' + '</div>' + '<div class="c-games_list_figure_info">' + '<time datetime="' + afterDate + '">' + dataitem[i].format_date + '</time>' + tagHtml + '<p class="' + linkClass + ' omit-sp">' + dataitem[i].title + '</p>' + '</div>' + '</a>' + '</li>';
      }
      return html;
    }
  }, {
    key: 'displayGamesTopics',
    value: function displayGamesTopics(data_a, data_b) {
      var json1 = data_a[0].item;
      var json2 = data_b[0].item;
      // jsonをマージ
      var json3 = json1.concat(json2);
      // 日付順にソート
      json3.sort(function (a, b) {
        if (a.date < b.date) return 1;
        if (a.date > b.date) return -1;
        return 0;
      });
      var searchItem = json3;
      var _this = this;
      var target = $('.c-games_listTime');
      // 初期表示数
      var l = 15;
      target.html('');
      $('.c-games_button-02-arrow').on('click', function () {
        $('.c-games_listTime li').remove();
        var checkList = $('input[name=radio]:checked').map(function () {
          return $(this).val();
        }).get();
        var checkDay = $('.p-games_form_selectWrap option:selected').map(function () {
          return $(this).val();
        }).get();
        var checkYear = Number(checkDay[0]);
        var checkMonth = checkDay[1];
        var category = [];
        for (var i = 0; i < l; i++) {
          var tag = searchItem[i].news_type;
          if (i >= searchItem.length) {
            $('.c-button_wrap').parent('div').remove();
          }
        }
        if (checkList[0] == 'all') {
          checkData = searchItem;
        } else {
          var filterSelect = searchItem.filter(function (searchItem) {
            var select_data = searchItem.type;
            return select_data == checkList[0];
          });
          checkData = filterSelect;
        }
        if (checkYear !== 999999) {
          var filterYear = checkData.filter(function (index) {
            var dataYear = index.date.match(/\d{1,4}/);
            return dataYear[0] == checkYear;
          });
          filterYear.sort(function (a, b) {
            return a.date < b.date ? 1 : -1;
          });
          checkData = filterYear;
        } else {
          checkData.sort(function (a, b) {
            return a.date < b.date ? 1 : -1;
          });
        }
        if (checkMonth !== "999999") {
          var filterMonth = checkData.filter(function (index2) {
            var dataMonth = index2.date.match(/\/\d{1,2}/);
            return dataMonth[0] == checkMonth;
          });
          filterMonth.sort(function (a, b) {
            return a.date < b.date ? 1 : -1;
          });
          checkData = filterMonth;
          $(target).append(_this.createHtmlGamesTopics(json3, l, checkData));
        } else if ("999999" == checkMonth) {
          checkData.sort(function (a, b) {
            return a.date < b.date ? 1 : -1;
          });
          $(target).append(_this.createHtmlGamesTopics(json3, l, checkData));
        }
        $(document).off('click', '.c-games_button-02-plus');
        $(document).on('click', '.c-games_button-02-plus', function () {
          l2 += 15;
          $(target).find('li').remove();
          $(target).append(_this.createHtmlGamesTopics(json3, l, checkData));
          if (l2 >= checkData.length) {
            $('.c-button_wrap').parent('div').remove();
          }
        });
        $(document).off('click', '.c-games_button-02-arrow');
        $(document).on('click', '.c-games_button-02-arrow', function () {
          l2 = 15;
          $('.c-games_button-02-arrow')[0].click();
          l2 = 15;
        });
      });

      $(target).append(_this.createHtmlGamesTopics(json3, l));
      $('.c-games_button-02-arrow')[0].click();
    }
  }, {
    key: 'createHtmlGamesTopics',
    value: function createHtmlGamesTopics(data, l) {
      var urlArr = location.pathname.split('/');
      var html = '';
      var dataitem = data;
      if (checkData !== null) {
        $('.c-button_wrap').parent('div').remove();
        dataitem = checkData;
        if (l2 > checkData.length) {
          l2 = checkData.length;
        }
      }
      if (l2 < 15) {
        $('.c-button_wrap').parent('div').remove();
      } else {
        if (location.pathname.match(/jp/)) {
          $('.l-games_content_inner').append('<div class="u-mt80-30 u-align-center"><div class="c-button_wrap"><div class="c-games_button-02-plus"><span>もっと見る</span></div></div></div>');
        } else if (location.pathname.match(/en/)) {
          $('.l-games_content_inner').append('<div class="u-mt80-30 u-align-center"><div class="c-button_wrap"><div class="c-games_button-02-plus"><span>See More</span></div></div></div>');
        }
      }
      if (checkData !== null) {
        if (checkData.length == 15) {
          $('.c-button_wrap').parent('div').remove();
        }
      }
      for (var i = 0; i < l2; i++) {
        var tag = dataitem[i].sub_type;
        var tagHtml = '';
        var tagarr = {
          'UPDATE': 'c-games_tag',
          'EVENTS': 'c-games_tag-02',
          'CAMPAIGN': 'c-games_tag-03',
          'INFORMATION': 'c-games_tag-04',
          'WEB': 'c-tag',
          'イベント': 'c-tag-02',
          '雑誌': 'c-tag-03',
          '新聞': 'c-tag-04',
          'テレビ': 'c-tag-05'
        };
        for (var j in tag) {
          if (j >= tag.length) break;
          tagHtml += '<span class="' + tagarr[tag[j].toUpperCase()] + '">' + tag[j] + '</span>';
        }
        var linkTarget = dataitem[i].new_target;
        var linkClass = urlArr[urlArr.length - 2] === 'klabgames' && linkTarget === '_blank' ? 'c-games_link-win' : linkTarget === '_blank' ? 'c-games_list_link c-games_link-win' : 'c-games_link-arrow-02 c-games_link-arrow-pc';
        var thum = dataitem[i].thumbnail_link ? dataitem[i].thumbnail_link : '/jp/assets/img/top/news03.png';
        var listClass = dataitem[i].link ? 'c-games_list_itemFigure' : 'c-games_list_itemFigure_noLink';
        var beforeDate = dataitem[i].format_date;
        beforeDate = beforeDate.split('/');
        var afterDate = beforeDate[0] + '-' + beforeDate[1] + '-' + beforeDate[2];

        html += '<li class="' + listClass + '">' + '<a href="' + dataitem[i].link + '" target="' + linkTarget + '">' + '<div class="c-games_list_figure">' + '<img src="' + thum + '" alt="' + dataitem[i].title + '" width="156" height="156">' + '</div>' + '<div class="c-games_list_figure_info">' + '<time datetime="' + afterDate + '">' + dataitem[i].format_date + '</time>' + tagHtml + '<p class="' + linkClass + ' omit-sp">' + dataitem[i].title + '</p>' + '</div>' + '</a>' + '</li>';
      }
      return html;
    }
  }]);

  return output_json;
}();

/* harmony default export */ __webpack_exports__["a"] = (output_json);

/***/ })
/******/ ]);
//# sourceMappingURL=script_games.js.map