// config
const config = require('./config.js');

// directory
const srcDir = config.directory.source;
const publicDir = config.directory.public;

// develop flag
const develop = config.develop;

// package
const gulp = require('gulp');
const browserSync = require('browser-sync');
const ssi = require('gulp-connect-ssi');
const sass = require('gulp-sass');
const sassGlob = require('gulp-sass-glob');
const sourcemaps = require('gulp-sourcemaps');
const gulpif = require('gulp-if');
const plumber = require('gulp-plumber');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const cleanCss = require('gulp-clean-css');
const webpackStream = require('webpack-stream');
const webpack = require('webpack');
const webpackConfig = (develop)? require('./webpack.config.dev') : require('./webpack.config');
const del = require('del');


/* --------------------------------------------
 Build Local Server task
-------------------------------------------- */
gulp.task('server', () => {
  browserSync({
    notify: false,
    server: {
      baseDir: publicDir,
      middleware: [
        ssi({
          baseDir: publicDir,
          ext: '.html'
        })
      ]
    }
  })
});


/* --------------------------------------------
 Build SCSS task (script.css)
-------------------------------------------- */
gulp.task('sass', () => {
  let entry = config.css.style;
  let output = config.css.output;
  let browsers = config.css.browsers;

  gulp.src(entry)
      .pipe(sassGlob())
      .pipe(gulpif(develop, sourcemaps.init()))
      .pipe(plumber())
      .pipe(sass())
      .pipe(autoprefixer({
        browsers: browsers,
        cascade: false
        })
      )
      .pipe(cleanCss())
      .pipe(gulpif(develop, sourcemaps.write('./')))
      .pipe(gulp.dest(output))
      .pipe(browserSync.stream());
});


/* --------------------------------------------
 Concat CSS task (vendor.css)
-------------------------------------------- */
gulp.task('concat', () => {
  let entry = config.css.vendor;
  let output = config.css.output;

  gulp.src(entry)
      .pipe(concat('vendor.css'))
      .pipe(cleanCss())
      .pipe(gulp.dest(output));
});


/* --------------------------------------------
 Build JS task
-------------------------------------------- */
gulp.task('js', () => {
  return webpackStream(webpackConfig, webpack)
  .pipe(gulp.dest(webpackConfig.output.path));
});


/* --------------------------------------------
 Gulp watch task
-------------------------------------------- */
gulp.task('watch', () => {
  gulp.watch(srcDir + '/**/*.scss', ['sass']);
  gulp.watch(srcDir + '/**/*.js', ['js']);
});


/* --------------------------------------------
 Del file
-------------------------------------------- */
gulp.task('del', (cb) => {
  let delList = config.del.list;
  del(delList, cb);
});


/* --------------------------------------------
 Gulp default task
-------------------------------------------- */
gulp.task('default', ['server', 'sass', 'concat', 'js', 'watch']);

