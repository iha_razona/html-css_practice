// config
const config = require('./config.js');
const entryScript = config.js.script;
const entryVendor = config.js.vendor;
const output = config.js.output;

module.exports = {
  entry: {
    script: entryScript,
    vendor: entryVendor
  },
  output: {
    path: output,
    filename: '[name].js'
  },
  devtool : 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                ['es2015', {'modules': false}]
              ]
            }
          }
        ]
      }
    ]
  }
}