export default class common {
	checkMedia() {
		const media = (window.matchMedia('(max-width:767px)').matches) ? 'sp' : 'pc';
		return media;
	}

	change(media) {
		let before = (media === 'sp') ? '_pc' : '_sp';
		let after = (media === 'sp') ? '_sp' : '_pc';

		let myClass = document.getElementsByClassName('changeImg');
		for (let i = 0; i < myClass.length; i++) {
			let src = myClass[i].getAttribute('src');
			let newSrc = src.replace(before, after);
			myClass[i].setAttribute('src', newSrc);
		}
	}
}